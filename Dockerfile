FROM python:3.10-alpine

MAINTAINER James Skeoch

EXPOSE 8000

RUN apk add --no-cache gcc python3-dev musl-dev postgresql-dev

ADD . /signup_service

WORKDIR /signup_service

RUN pip install --no-cache-dir -r requirements.txt

RUN python manage.py makemigrations

RUN python manage.py migrate

CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]
