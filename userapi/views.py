# views.py
from rest_framework import viewsets
from rest_framework_api_key.permissions import HasAPIKey

from .serializers import UserSerializer
from .models import User


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [HasAPIKey]
    queryset = User.objects.all().order_by('first_name')
    serializer_class = UserSerializer
